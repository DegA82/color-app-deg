package com.rave.colorapp

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.rave.colorapp.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    // TODO: Define the lazy delegate 
    private val binding by lazy { ActivityMainBinding.inflate(layoutInflater) }

    // TODO: What does the override keyword do
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)

        // TODO: What category of kotlin function does with() belong too
        with(binding) {
            cvBlue.setOnClickListener { Intent(this@MainActivity,BlueActivity::class.java).also{
                intent->
                startActivity(intent)
                }
            }
            cvGray.setOnClickListener { Intent(this@MainActivity,GreyActivity::class.java).also {
                intent ->
                startActivity(intent)
                }
            }
            cvRed.setOnClickListener { Intent(this@MainActivity,RedActivity::class.java).also{
                intent ->
                startActivity(intent)
                }
            }
            cvPurple.setOnClickListener {Intent(this@MainActivity,PurpleActivity::class.java).also {
                intent ->
                startActivity(intent)
                }
            }
            cvGreen.setOnClickListener { Intent(this@MainActivity,GreenActivity::class.java).also {
                intent->
                startActivity(intent)
                }
            }
            cvYellow.setOnClickListener {Intent(this@MainActivity,YellowActivity::class.java).also{
                intent ->
                startActivity(intent)
                }
            }
        }
    }

    // TODO: What kind of function is this?
    private fun String.toast() {
        Toast.makeText(this@MainActivity, this, Toast.LENGTH_SHORT).show()
    }
}